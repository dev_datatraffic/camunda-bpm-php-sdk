<?php

namespace org\camunda\php\sdk\entity\response;

use org\camunda\php\sdk\helper\CastHelper;

class MigrationPlan extends CastHelper{
    protected $sourceProcessDefinitionId;
    protected $targetProcessDefinitionId;
    protected $instructions;

    /**
     * @return mixed
     */
    public function getSourceProcessDefinitionId()
    {
        return $this->sourceProcessDefinitionId;
    }

    /**
     * @return mixed
     */
    public function getTargetProcessDefinitionId()
    {
        return $this->targetProcessDefinitionId;
    }

    /**
     * @return mixed
     */
    public function getInstructions()
    {
        return $this->instructions;
    }
}
