<?php
namespace org\camunda\php\sdk\entity\response;
use org\camunda\php\sdk\helper\CastHelper;

class Incident extends CastHelper {
    protected $id;
    protected $processDefinitionId;
    protected $processInstanceId;
    protected $executionId;
    protected $incidentTimestamp;
    protected $incidentType;
    protected $activityId;
    protected $causeIncidentId;
    protected $rootCauseIncidentId;
    protected $configuration;
    protected $tenantId;
    protected $incidentMessage;
    protected $jobDefinitionId;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getProcessDefinitionId()
    {
        return $this->processDefinitionId;
    }

    /**
     * @param mixed $processDefinitionId
     */
    public function setProcessDefinitionId($processDefinitionId): void
    {
        $this->processDefinitionId = $processDefinitionId;
    }

    /**
     * @return mixed
     */
    public function getProcessInstanceId()
    {
        return $this->processInstanceId;
    }

    /**
     * @param mixed $processInstanceId
     */
    public function setProcessInstanceId($processInstanceId): void
    {
        $this->processInstanceId = $processInstanceId;
    }

    /**
     * @return mixed
     */
    public function getExecutionId()
    {
        return $this->executionId;
    }

    /**
     * @param mixed $executionId
     */
    public function setExecutionId($executionId): void
    {
        $this->executionId = $executionId;
    }

    /**
     * @return mixed
     */
    public function getIncidentTimestamp()
    {
        return $this->incidentTimestamp;
    }

    /**
     * @param mixed $incidentTimestamp
     */
    public function setIncidentTimestamp($incidentTimestamp): void
    {
        $this->incidentTimestamp = $incidentTimestamp;
    }

    /**
     * @return mixed
     */
    public function getIncidentType()
    {
        return $this->incidentType;
    }

    /**
     * @param mixed $incidentType
     */
    public function setIncidentType($incidentType): void
    {
        $this->incidentType = $incidentType;
    }

    /**
     * @return mixed
     */
    public function getActivityId()
    {
        return $this->activityId;
    }

    /**
     * @param mixed $activityId
     */
    public function setActivityId($activityId): void
    {
        $this->activityId = $activityId;
    }

    /**
     * @return mixed
     */
    public function getCauseIncidentId()
    {
        return $this->causeIncidentId;
    }

    /**
     * @param mixed $causeIncidentId
     */
    public function setCauseIncidentId($causeIncidentId): void
    {
        $this->causeIncidentId = $causeIncidentId;
    }

    /**
     * @return mixed
     */
    public function getRootCauseIncidentId()
    {
        return $this->rootCauseIncidentId;
    }

    /**
     * @param mixed $rootCauseIncidentId
     */
    public function setRootCauseIncidentId($rootCauseIncidentId): void
    {
        $this->rootCauseIncidentId = $rootCauseIncidentId;
    }

    /**
     * @return mixed
     */
    public function getConfiguration()
    {
        return $this->configuration;
    }

    /**
     * @param mixed $configuration
     */
    public function setConfiguration($configuration): void
    {
        $this->configuration = $configuration;
    }

    /**
     * @return mixed
     */
    public function getTenantId()
    {
        return $this->tenantId;
    }

    /**
     * @param mixed $tenantId
     */
    public function setTenantId($tenantId): void
    {
        $this->tenantId = $tenantId;
    }

    /**
     * @return mixed
     */
    public function getIncidentMessage()
    {
        return $this->incidentMessage;
    }

    /**
     * @param mixed $incidentMessage
     */
    public function setIncidentMessage($incidentMessage): void
    {
        $this->incidentMessage = $incidentMessage;
    }

    /**
     * @return mixed
     */
    public function getJobDefinitionId()
    {
        return $this->jobDefinitionId;
    }

    /**
     * @param mixed $jobDefinitionId
     */
    public function setJobDefinitionId($jobDefinitionId): void
    {
        $this->jobDefinitionId = $jobDefinitionId;
    }
}