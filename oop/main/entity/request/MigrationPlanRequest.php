<?php

namespace org\camunda\php\sdk\entity\request;

class MigrationPlanRequest extends Request {

    protected $sourceProcessDefinitionId;
    protected $targetProcessDefinitionId;
    protected $updateEventTriggers;

    /**
     * @return mixed
     */
    public function getSourceProcessDefinitionId()
    {
        return $this->sourceProcessDefinitionId;
    }

    /**
     * @param mixed $sourceProcessDefinitionId
     */
    public function setSourceProcessDefinitionId($sourceProcessDefinitionId)
    {
        $this->sourceProcessDefinitionId = $sourceProcessDefinitionId;
    }

    /**
     * @return mixed
     */
    public function getTargetProcessDefinitionId()
    {
        return $this->targetProcessDefinitionId;
    }

    /**
     * @param mixed $targetProcessDefinitionId
     */
    public function setTargetProcessDefinitionId($targetProcessDefinitionId)
    {
        $this->targetProcessDefinitionId = $targetProcessDefinitionId;
    }

    /**
     * @return mixed
     */
    public function getUpdateEventTriggers()
    {
        return $this->updateEventTriggers;
    }

    /**
     * @param mixed $updateEventTriggers
     */
    public function setUpdateEventTriggers($updateEventTriggers)
    {
        $this->updateEventTriggers = $updateEventTriggers;
    }
}
