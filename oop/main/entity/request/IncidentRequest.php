<?php

namespace org\camunda\php\sdk\entity\request;

class IncidentRequest extends Request
{
    protected $incidentType;
    protected $incidentMessage;
    protected $processDefinitionId;
    protected $processInstanceId;
    protected $executionId;
    protected $activityId;
    protected $causeIncidentId;
    protected $rootCauseIncidentId;
    protected $configuration;
    protected $tenantIdIn;
    protected $jobDefinitionIdIn;
    protected $sortBy;
    protected $sortOrder;

    /**
     * @return mixed
     */
    public function getIncidentType()
    {
        return $this->incidentType;
    }

    /**
     * @param mixed $incidentType
     */
    public function setIncidentType($incidentType)
    {
        $this->incidentType = $incidentType;
    }

    /**
     * @return mixed
     */
    public function getIncidentMessage()
    {
        return $this->incidentMessage;
    }

    /**
     * @param mixed $incidentMessage
     */
    public function setIncidentMessage($incidentMessage)
    {
        $this->incidentMessage = $incidentMessage;
    }

    /**
     * @return mixed
     */
    public function getProcessDefinitionId()
    {
        return $this->processDefinitionId;
    }

    /**
     * @param mixed $processDefinitionId
     */
    public function setProcessDefinitionId($processDefinitionId)
    {
        $this->processDefinitionId = $processDefinitionId;
    }

    /**
     * @return mixed
     */
    public function getProcessInstanceId()
    {
        return $this->processInstanceId;
    }

    /**
     * @param mixed $processInstanceId
     */
    public function setProcessInstanceId($processInstanceId)
    {
        $this->processInstanceId = $processInstanceId;
    }

    /**
     * @return mixed
     */
    public function getExecutionId()
    {
        return $this->executionId;
    }

    /**
     * @param mixed $executionId
     */
    public function setExecutionId($executionId)
    {
        $this->executionId = $executionId;
    }

    /**
     * @return mixed
     */
    public function getActivityId()
    {
        return $this->activityId;
    }

    /**
     * @param mixed $activityId
     */
    public function setActivityId($activityId)
    {
        $this->activityId = $activityId;
    }

    /**
     * @return mixed
     */
    public function getCauseIncidentId()
    {
        return $this->causeIncidentId;
    }

    /**
     * @param mixed $causeIncidentId
     */
    public function setCauseIncidentId($causeIncidentId)
    {
        $this->causeIncidentId = $causeIncidentId;
    }

    /**
     * @return mixed
     */
    public function getRootCauseIncidentId()
    {
        return $this->rootCauseIncidentId;
    }

    /**
     * @param mixed $rootCauseIncidentId
     */
    public function setRootCauseIncidentId($rootCauseIncidentId)
    {
        $this->rootCauseIncidentId = $rootCauseIncidentId;
    }

    /**
     * @return mixed
     */
    public function getConfiguration()
    {
        return $this->configuration;
    }

    /**
     * @param mixed $configuration
     */
    public function setConfiguration($configuration)
    {
        $this->configuration = $configuration;
    }

    /**
     * @return mixed
     */
    public function getTenantIdIn()
    {
        return $this->tenantIdIn;
    }

    /**
     * @param mixed $tenantIdIn
     */
    public function setTenantIdIn($tenantIdIn)
    {
        $this->tenantIdIn = $tenantIdIn;
    }

    /**
     * @return mixed
     */
    public function getJobDefinitionIdIn()
    {
        return $this->jobDefinitionIdIn;
    }

    /**
     * @param mixed $jobDefinitionIdIn
     */
    public function setJobDefinitionIdIn($jobDefinitionIdIn)
    {
        $this->jobDefinitionIdIn = $jobDefinitionIdIn;
    }

    /**
     * @return mixed
     */
    public function getSortBy()
    {
        return $this->sortBy;
    }

    /**
     * @param mixed $sortBy
     */
    public function setSortBy($sortBy)
    {
        $this->sortBy = $sortBy;
    }

    /**
     * @return mixed
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * @param mixed $sortOrder
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;
    }
}