<?php

namespace org\camunda\php\sdk\entity\request;

class MigrationRequest extends Request {

    protected $migrationPlan;
    protected $processInstanceIds;
    protected $processInstanceQuery;
    protected $skipCustomListeners;
    protected $skipIoMappings;

    /**
     * @return mixed
     */
    public function getMigrationPlan()
    {
        return $this->migrationPlan;
    }

    /**
     * @param mixed $migrationPlan
     */
    public function setMigrationPlan($migrationPlan)
    {
        $this->migrationPlan = $migrationPlan;
    }

    /**
     * @return mixed
     */
    public function getProcessInstanceIds()
    {
        return $this->processInstanceIds;
    }

    /**
     * @param mixed $processInstanceIds
     */
    public function setProcessInstanceIds($processInstanceIds)
    {
        $this->processInstanceIds = $processInstanceIds;
    }

    /**
     * @return mixed
     */
    public function getProcessInstanceQuery()
    {
        return $this->processInstanceQuery;
    }

    /**
     * @param mixed $processInstanceQuery
     */
    public function setProcessInstanceQuery($processInstanceQuery)
    {
        $this->processInstanceQuery = $processInstanceQuery;
    }

    /**
     * @return mixed
     */
    public function getSkipCustomListeners()
    {
        return $this->skipCustomListeners;
    }

    /**
     * @param mixed $skipCustomListeners
     */
    public function setSkipCustomListeners($skipCustomListeners)
    {
        $this->skipCustomListeners = $skipCustomListeners;
    }

    /**
     * @return mixed
     */
    public function getSkipIoMappings()
    {
        return $this->skipIoMappings;
    }

    /**
     * @param mixed $skipIoMappings
     */
    public function setSkipIoMappings($skipIoMappings)
    {
        $this->skipIoMappings = $skipIoMappings;
    }
}
