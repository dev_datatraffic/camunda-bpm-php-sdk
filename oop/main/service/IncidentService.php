<?php
namespace org\camunda\php\sdk\service;

use Exception;
use org\camunda\php\sdk\entity\request\IncidentRequest;
use org\camunda\php\sdk\entity\response\Incident;

class IncidentService extends RequestService {

    public function getIncidents(IncidentRequest $request) {
        $this->setRequestUrl('/incident');
        $this->setRequestObject($request);
        $this->setRequestMethod('GET');

        try {
            $prepare = $this->execute();
            $response = array();

            foreach ($prepare AS $index => $data) {
                $incident = new Incident();
                $response['incident_' . $index] = $incident->cast((object)$data);
            }
            return (object)$response;
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function getCount(IncidentRequest $request) {
        $this->setRequestUrl('/incident/count');
        $this->setRequestObject($request);
        $this->setRequestMethod('GET');

        try {
            return $this->execute()->count;
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function deleteIncident($id)
    {
        $this->setRequestUrl("/incident/{$id}");
        $this->setRequestObject(null);
        $this->setRequestParams(null);
        $this->setRequestMethod('DELETE');

        try {
            $this->execute();
        } catch (Exception $e) {
            throw $e;
        }
    }
}