<?php
namespace org\camunda\php\sdk\service;

use Exception;
use org\camunda\php\sdk\entity\request\MigrationPlanRequest;
use org\camunda\php\sdk\entity\request\MigrationRequest;
use org\camunda\php\sdk\entity\response\MigrationPlan;

class MigrationService extends RequestService
{
    public function generateMigrationPlan(MigrationPlanRequest $request)
    {
        $migrationPlan = new MigrationPlan();
        $this->setRequestUrl('/migration/generate');
        $this->setRequestMethod('POST');
        $this->setRequestObject($request);

        try {
            return $migrationPlan->cast($this->execute());
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function executeMigrationPlan(MigrationRequest $request)
    {
        $this->setRequestUrl('/migration/execute');
        $this->setRequestMethod('POST');
        $this->setRequestObject($request);
        $this->execute();
    }
}
