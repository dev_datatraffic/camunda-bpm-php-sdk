<?php


namespace org\camunda\php\sdk\service;

use Exception;
use org\camunda\php\sdk\entity\request\DeploymentRequest;
use org\camunda\php\sdk\entity\response\Deployment;

class DeploymentService extends RequestService
{
    /**
     * Create a Deployment
     *
     * @param DeploymentRequest $request
     * @return void
     */
    public function create(DeploymentRequest $request)
    {
        $deployment = new Deployment();
        $this->setRequestUrl("/deployment/create");
        $this->setRequestObject($request);
        $this->setRequestMethod('POST');

        try {
            return $deployment->cast($this->executeUploadFile());
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function redeploy()
    { }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function list()
    { }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function getListCount()
    { }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function get()
    { }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function getResources()
    { }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function getResource()
    { }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function getResourceBinary()
    { }

    /**
     * Removes a Delete Deployment
     * @link https://docs.camunda.org/manual/7.10/reference/rest/deployment/delete-deployment/
     *
     * @param String $id deployment Id
     * @param DeploymentRequest $request
     * @param Array $params
     * @throws \Exception
     * @return Status 204. No content.
     */
    public function deleteDeployment($id, DeploymentRequest $request, $params = [])
    {
        $this->setRequestUrl("/deployment/{$id}");
        $this->setRequestObject($request);
        $this->setRequestParams($params);
        $this->setRequestMethod('DELETE');

        try {
            $this->execute();
        } catch (Exception $e) {
            throw $e;
        }
    }
}