<?php

/**
 * Created by IntelliJ IDEA.
 * User: hentschel
 * Date: 07.06.13
 * Time: 20:43
 * To change this template use File | Settings | File Templates.
 */

namespace org\camunda\php\sdk\service;

use GuzzleHttp\Client;
use GuzzleHttp\Middleware;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\MessageFormatter;
use Illuminate\Support\Facades\Config;
use JSON;
use Monolog\Logger;
use Monolog\Handler\RotatingFileHandler;
use org\camunda\php\sdk\entity\request\CredentialsRequest;
use org\camunda\php\sdk\entity\request\ProfileRequest;
use org\camunda\php\sdk\entity\request\Request;
use org\camunda\php\sdk\entity\request\VariableRequest;

class RequestService
{
    private $requestObject;
    private $requestParams = [];
    private $requestMethod = "GET";
    private $requestUrl;
    private $httpStatusCode;
    private $restApiUrl;
    private $headers = [
        'Content-Type' => 'application/json',
    ];

    public function __construct($restApiUrl)
    {
        /*$format = "[%datetime%] %extra.process_id% %level_name%: %message%\n";
        $allowInlineLineBreaks = false;
        $ignoreEmptyContextAndExtra = true;
        $includeStacktraces = null;
        $dateFormat = "Y-m-d H:i:s";
        $formatter = new \Monolog\Formatter\LineFormatter($format, $dateFormat, $allowInlineLineBreaks, $ignoreEmptyContextAndExtra);

        $daysLog = Config::get('camunda.log.max_files', 0);
        $handler = new RotatingFileHandler(Config::get('camunda.log.directory').'camunda.log', $daysLog, \Monolog\Logger::DEBUG, true, 0664);

        $handler->setFormatter($formatter);
        $monologHandler = new Logger('Webhook', [$handler]);

        $stack = HandlerStack::create();
        $stack->push(Middleware::log($monologHandler, new MessageFormatter('{method} - {uri} - {code} - {req_body} - {res_body}')));*/

        $this->restApiUrl = $restApiUrl;
        $this->headers = ['Content-Type' => 'application/json'];

        $this->client = new Client([
            'base_uri' => $this->restApiUrl,
            'headers' => $this->getHeaders(),
            //'handler' => $stack
        ]);


    }

    /**
     * Get the value of headers
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * Set the value of headers
     *
     * @return  self
     */
    public function setHeaders($headers)
    {
        $this->headers = $headers;

        return $this;
    }

    /**
     * @param Request $requestObject
     */
    protected function setRequestObject(Request $requestObject = null)
    {
        $this->requestObject = $requestObject;
    }

    /**
     * @return mixed
     */
    protected function getRequestObject()
    {
        return $this->requestObject;
    }

    /**
     * @return mixed
     */
    protected function getRequestParams()
    {
        $params = null;
        if (is_array($this->requestParams)) {
            $tmp = [];

            $objTmp = new \ReflectionClass(get_class($this->getRequestObject()));
            foreach ($this->getRequestObject()->iterate() as $key => $value) {
                if ($value != null && !empty($value) && in_array($key, $this->requestParams)) {
                    $tmp[] = $key . "=" . $value;
                    $property = $objTmp->getProperty($key);
                    $property->setAccessible(true);
                    $property->setValue($this->getRequestObject(), null);
                }
            }
            $params = implode('&', $tmp);
            $params = "?{$params}";
        }

        return $params;
    }

    /**
     * @param Request $requestParams
     */
    protected function setRequestParams($requestParams = [])
    {
        $this->requestParams = $requestParams;
    }

    /**
     * @param mixed $requestMethod
     */
    protected function setRequestMethod($requestMethod)
    {
        $this->requestMethod = $requestMethod;
    }

    /**
     * @return mixed
     */
    protected function getRequestMethod()
    {
        return strtoupper($this->requestMethod);
    }

    /**
     * @param $requestUrl
     */
    protected function setRequestUrl($requestUrl)
    {
        $this->requestUrl = $requestUrl;
    }

    /**
     * @return mixed
     */
    protected function getRequestUrl()
    {
        $baseURI = preg_replace('/\/$/', '', $this->restApiUrl);
        return $baseURI . $this->requestUrl;
    }

    /**
     * @param mixed $httpStatusCode
     */
    public function setHttpStatusCode($httpStatusCode)
    {
        $this->httpStatusCode = $httpStatusCode;
    }

    /**
     * @return mixed
     */
    public function getHttpStatusCode()
    {
        return $this->httpStatusCode;
    }

    /**
     * executes the rest request
     *
     * @throws \Exception
     * @return mixed server response
     */
    protected function execute()
    {
        switch ($this->getRequestMethod()) {
            case 'OPTIONS':
                $response = $this->client->request(
                    $this->getRequestMethod(),
                    $this->getRequestUrl()
                );
                $this->setStatusCode($response);
                break;

            case 'PUT':
            case 'POST':
            case 'DELETE':
                $response = $this->client->request(
                    $this->getRequestMethod(),
                    $this->getRequestUrl() . $this->getRequestParams(),
                    [
                        'headers' => $this->getHeaders(),
                        'json' => $this->getData() ?? []
                    ]
                );
                $this->setStatusCode($response);

                break;
            case 'GET':
            default:
                $response = $this->client->request(
                    $this->getRequestMethod(),
                    $this->getRequestUrl() . $this->getData()
                );
                $this->setStatusCode($response);

                break;
        }

        if (preg_match('/(^10|^20)[0-9]/', $this->getStatusCode())) {
            $this->reset();
            return (object) json_decode($response->getBody()->getContents(), true);
        } else {
            $this->reset();
            if ($response != null && $response != " " && !empty($response)) {
                $error = $response;
            } else {
                $error = new \stdClass();
                $error->type = "Not found!";
                $error->message = "Not Message!";
            }
            throw new \Exception("Error! HTTP Status Code: " . $this->getStatusCode() . " --ErrorType: " . $error->type . " --Error Message: " . $error->message);
        }
    }

    protected function executeUploadFile()
    {
        if (in_array($this->getRequestMethod(), ['PUT', 'POST'])) {

            $response = $this->client->request(
                $this->getRequestMethod(),
                $this->getRequestUrl(),
                ['multipart' => $this->getData()]
            );
            $this->setStatusCode($response);

            if (preg_match('/(^10|^20)[0-9]/', $this->getStatusCode())) {
                $this->reset();
                return (object) json_decode($response->getBody()->getContents());
            } else {
                $this->reset();
                if ($response != null && $response != "" && !empty($response)) {
                    $error = $response;
                } else {
                    $error = new \stdClass();
                    $error->type = "Not found!";
                    $error->message = "Not Message!";
                }
                throw new \Exception("Error!HTTP Status Code: " . $this->getStatusCode() . " --ErrorType: " . $error->type . " --Error Message: " . $error->message);
            }
        }
    }

    /**
     * Reset
     *
     * @return void
     */
    private function reset()
    {
        $this->setRequestObject(null);
        $this->setRequestUrl('');
        $this->setRequestMethod('GET');
    }

    /**
     * getStatusCode
     *
     * @return void
     */
    protected function getStatusCode()
    {
        return $this->httpStatusCode;
    }

    /**
     * Set Status Code
     *
     * @return void
     */
    protected function setStatusCode($response)
    {
        $this->httpStatusCode = $response->getStatusCode();
    }

    /**
     * Parse Data
     *
     * @param [type] $data
     * @param [type] $json
     * @return void
     */
    protected function getData()
    {
        $tmp = array();
        if (in_array($this->getRequestMethod(), ['POST', 'PUT', 'DELETE'])) {

            if ($this->getRequestObject()) {

                foreach ($this->getRequestObject()->iterate() as $index => $value) {
                    if ($value != null && !empty($value)) {
                        // We need to change the Objects of Profile and Credentials into an Array
                        if ($value instanceof ProfileRequest || $value instanceof CredentialsRequest) {
                            $objArray = array();
                            foreach ($value->iterate() as $i => $d) {
                                if (!empty($d)) {
                                    $objArray[$i] = $d;
                                }
                            }
                            $value = $objArray;
                        }

                        // Needed for Modifications and Deletions in VariableRequest
                        // Changes Array Data into a new Array if these are instances of VariableRequest
                        if (is_array($value)) {
                            foreach ($value as $valueIndex => $valueData) {
                                if ($valueData instanceof VariableRequest) {
                                    $objArray = array();
                                    foreach ($valueData->iterate() as $i => $d) {
                                        if (!empty($d)) {
                                            $objArray[$i] = $d;
                                        }
                                    }
                                    $valueData = $objArray;
                                }
                                $value[$valueIndex] = $valueData;
                            }
                        }
                        $tmp[$index] = $value;
                    }
                }
            }

            if (empty($tmp)) {
                $tmp = new \stdClass();
            }

            $data = $tmp;
        } else {
            $data = '?';
            $tmp = [];
            if (isset($this->requestObject)) {
                foreach ($this->requestObject->iterate() as $index => $value) {
                    if ($value != null && !empty($value)) {
                        $tmp[] = $index . '=' . $value;
                    }
                }
            }

            $data .= implode('&', $tmp);
        }

        return $data;
    }
}
